def code_morze(text):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    morze_code = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.',
        'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---',
        'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---',
        'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--',
        'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-',
        '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.',
        '0': '-----', ',': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.',
        '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }
    words = text.upper().split()
    morze_words = []
    for word in words:
        morze_word = ""
        for char in word:
            if char in morze_code:
                morze_word =morze_word+ morze_code[char] + " "
        morze_words.append(morze_word.strip())
    morze_message = " ".join(morze_words)  
    return morze_message
input_text = "Data Science - 2023"
morze_result = code_morze(input_text)
print(morze_result)

